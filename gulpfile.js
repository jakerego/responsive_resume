/*!
 * gulp
 * $ npm install gulp-ruby-sass gulp-autoprefixer gulp-minify-css gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-cache del --save-dev
 */

"use strict";
  
// Load plugins
var gulp =            require('gulp'),
    sass =            require('gulp-ruby-sass'),
    autoprefixer =    require('gulp-autoprefixer'),
    minifycss =       require('gulp-minify-css'),
    jshint =          require('gulp-jshint'),
    uglify =          require('gulp-uglify'),
    imagemin =        require('gulp-imagemin'),
    rename =          require('gulp-rename'),
    concat =          require('gulp-concat'),
    clean =           require('gulp-clean'),
    notify =          require('gulp-notify'),
    cache =           require('gulp-cache'),
    build =           require('gulp-build'),
    exec =            require('gulp-exec'),
    del =             require('del'),
 	  browserSync =     require('browser-sync').create(),
    runSequence =     require('run-sequence'),
    clean =           require('gulp-clean'),
    util =            require('gulp-util'),
    gulpif =          require('gulp-if'),
    args =            require('yargs').argv,
    isDev = args.env === 'development';

// configuration
var config = {
  sourcefiles: "./src",
  compiled:    "./dist",
  files: {
    styles: {
      sass: './src/css/style.scss',
      dist: './dist',
      css:  './dist/styles/style.css',
    },
    images: './src/images/**/*'
  }
};

// Browser Sync
gulp.task('browser-sync', function() {
    browserSync.init(['dist/styles/*.css', 'dist/js/*.js'], {
        server: {
            baseDir: './'
        }
    });
});

// compile sass (using Sass Ruby gem)
// includes autoprefix for better browser support
gulp.task('styles:sass', function() {
    return sass(config.files.styles.sass, {
        sourcemap: (isDev) ? true : false,
          lineNumber: (isDev) ? true : false
      })
      .on('error', function (err) {
          console.error('Error!', err.message);
      })
      .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
      .pipe(gulpif(isDev, sourcemaps.write('maps', {
          includeContent: false,
          sourceRoot: '.'
      })))
      .pipe(gulp.dest(config.files.styles.dist))
      .pipe(gulpif(isDev, browserSync.stream()));
});

// Styles
gulp.task('styles', function() {
  return sass('src/css/style.scss', { style: 'expanded' })
    .pipe(autoprefixer('last 2 version'))
    .pipe(gulp.dest('dist/styles'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest('dist/styles'))
    .pipe(notify({ message: 'Styles task complete' }));

});
 
// Scripts
gulp.task('scripts', function() {
  return gulp.src('src/js/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('dist/scripts'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('dist/scripts'))
    .pipe(notify({ message: 'Scripts task complete' }));
});
 
// Images
gulp.task('images', function() {
  return gulp.src('/src/images/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('/dist/images'))
    .pipe(notify({ message: 'Images task complete' }));
});
 
// server for compiled P.L. in "public" directory
gulp.task('serve', function() {
  browserSync.init({
    server: {
      baseDir: config.compiled
    }
  });
});

// server for compiled P.L. in "public" directory
gulp.task('clean', function(cb) {
  del('./dist/styles/**.scss', cb);
});

// default build
gulp.task('default', ['clean'], function() {
  // define build tasks
  var tasks = [
    'build',
    'styles'
  ];

  // add tasks if development
  if (isDev) {
    tasks.push('watch');
  }

  // run build and tasks
  runSequence(tasks, function() {
    // start watcher if "development"
    if (isDev) {
      gulp.start('serve');
    }
  });
});
  

gulp.task('build', function() {
  exec('php core/builder.php -g;', function() {
    if (isDev) {
      gulp.src(config.sourcefiles).pipe(browserSync.stream());
    }
  });
}); 

// run tasks when files are updated
gulp.task('watch', function() {
  gulp.task('styles:watch', ['styles']);
  gulp.watch('./src/css/*.scss', ['styles:watch']);

  gulp.task('build:watch', ['build', 'styles']);
  gulp.watch([
        './dist/styles/**.css',
        './src/js/**/*',
        './src/images/**/*',
        ], ['build:watch']);
});
